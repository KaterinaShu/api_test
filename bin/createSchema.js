'use strict'
const app = require('../server/server');
const ds = app.dataSources.db;
const lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];

ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log(
    'Loopback tables [' + lbTables + '] created in ',
    ds.adapter.name
  );
  ds.disconnect();
});
